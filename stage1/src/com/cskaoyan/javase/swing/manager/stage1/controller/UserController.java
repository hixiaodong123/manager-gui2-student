package com.cskaoyan.javase.swing.manager.stage1.controller;

import com.cskaoyan.javase.swing.manager.stage1.dao.UserDao;
import com.cskaoyan.javase.swing.manager.stage1.model.User;

/**
 * 与管理员用户相关的,业务操作的实现
 * @since 20:18
 * @author wuguidong@cskaoyan.onaliyun.com
 */
public class UserController {

    // 业务处理需要获取数据,所以需要依赖数据处理层
    private UserDao userDao = new UserDao();

    /**
     * 判断能否登陆，返回一个int状态值
     * 其中：
     * 0，表示正常成功登陆
     * 1，表示用户不存在
     * 2，表示密码输入错误
     * @since 20:26
     * @param user 用户输入用户对象
     * @return int 登陆状态判断
     * @author wuguidong@cskaoyan.onaliyun.com
     */
    public int judgeLogin(User user) {
        return 0;
    }
}
