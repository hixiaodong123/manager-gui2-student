package com.cskaoyan.javase.swing.manager.stage1.dao;

import com.cskaoyan.javase.swing.manager.stage1.model.User;
import com.cskaoyan.javase.swing.manager.stage1.model.UserData;

/**
 * 与管理员用户相关的，所有数据处理都在该类下进行
 *
 * @since 19:36
 * @author wuguidong@cskaoyan.onaliyun.com
 */
public class UserDao {

    // 从数据源获取数据
    private  User[] users = UserData.USERS;

}
