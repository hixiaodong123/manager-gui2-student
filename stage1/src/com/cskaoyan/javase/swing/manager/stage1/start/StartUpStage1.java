package com.cskaoyan.javase.swing.manager.stage1.start;

import com.cskaoyan.javase.swing.manager.stage1.view.LoginFrame;

/**
 * 从此启动整个程序
 * @since 14:50
 * @author wuguidong@cskaoyan.onaliyun.com
 */
public class StartUpStage1 {
    public static void main(String[] args) {
        new LoginFrame().setVisible(true);
    }
}
