package com.cskaoyan.javase.swing.manager.stage2.controller;

import com.cskaoyan.javase.swing.manager.stage2.model.Student;


/**
 * 与学生信息相关的,业务操作接口
 * @since 19:10
 * @author wuguidong@cskaoyan.onaliyun.com
 */
public interface StudentController {

    // 获取表格需要的学生信息二维数组
    String[][] getAllTableData();

    // 获取表格列数据
    String[] getTableColumns();

    // 检查学号重复性
    boolean checkStuIdRepeat(String id);

    // 插入一条学生信息
    boolean addStudent(Student stu);

}
