package com.cskaoyan.javase.swing.manager.stage2.controller.impl;

import com.cskaoyan.javase.swing.manager.stage2.controller.StudentController;
import com.cskaoyan.javase.swing.manager.stage2.dao.StudentDao;
import com.cskaoyan.javase.swing.manager.stage2.dao.impl.StudentDaoImpl;
import com.cskaoyan.javase.swing.manager.stage2.model.Student;

/**
 * 与学生信息相关的,业务操作的具体实现
 * @since 19:30
 * @author wuguidong@cskaoyan.onaliyun.com
 */
public class StudentControllerImpl implements StudentController {

    // 业务处理需要获取数据,所以需要依赖数据处理层
    private StudentDao studentDao = new StudentDaoImpl();

    /**
     * Swing GUI表格需要的表格数据data是一个String类型的二维数组,
     *      所以需要封装出二维数组
     * @since 9:19
     * @return java.lang.String[][] 作为GUI表格的数据data
     * @author wuguidong@cskaoyan.onaliyun.com
     */
    @Override
    public String[][] getAllTableData() {
        /*
            这里，由于你是第一次做，而且这里涉及二维数组
            所以为了避免你完全懵逼，给出一个范例，方便你理解。
            现在，你可以直接在包start下启动main方法，
            这样表格中就会有一条数据。
            也就是说下述代码案例已经告诉你，如何将只有一个学生对象的数组封装成String二维数组。接下来，就看你的了。

            在你开始写代码时，下述代码需要完全删除重新写实现。
         */

        Student student = new Student("1", "李明", "男", "北京大学", "软件工程", "18", "武汉", "13817618878", "123@qq.com");
        Student[] stus = {student};
        String[][] result = new String[stus.length][];
        // 遍历二维数组并赋值
        /*
            赋值很简单,因为这个二维数组的长度一定是学生数组的长度
            并且二维数组中的每个String一维数组数组的长度一定是表格的列数,是固定的，每个String数组对应位置内容也就是某列的内容，也是固定的。
         */
        for (int i = 0; i < result.length; i++) {
            Student aStu = stus[i];
            result[i] = new String[]{
                    aStu.getStuId(), aStu.getName(), aStu.getGender(), aStu.getSchool(), aStu.getMajor(), aStu.getAge(), aStu.getCity(), aStu.getPhone(), aStu.getEmail()};
        }
        return result;
    }

    /**
     *  Swing GUI表格需要的表格列是一个String数组
     * @since 9:20
     * @return java.lang.String[]
     * @author wuguidong@cskaoyan.onaliyun.com
     */
    @Override
    public String[] getTableColumns() {
        return studentDao.getTableColumns();
    }

    /**
     * 检查id是否重复,true表示重复,否则为不重复
     * @since 18:11
     * @param id 学生id
     * @return boolean
     * @author wuguidong@cskaoyan.onaliyun.com
     */
    @Override
    public boolean checkStuIdRepeat(String id) {
        return false;
    }

    /**
     * 导入一条学生信息，true表示成功，否则为失败
     * 由于已经判定过很多次了，所以失败只有一种可能，即数组满了
     * @since 14:21
     * @param stu GUI界面输入的学生对象
     * @return boolean
     * @author wuguidong@cskaoyan.onaliyun.com
     */
    @Override
    public boolean addStudent(Student stu) {
        return false;
    }

}
