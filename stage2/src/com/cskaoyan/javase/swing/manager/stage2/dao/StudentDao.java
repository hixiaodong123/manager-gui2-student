package com.cskaoyan.javase.swing.manager.stage2.dao;

/**
 * 与Student学生数据相关的，数据操作的接口
 *
 * @since 14:21
 * @author wuguidong@cskaoyan.onaliyun.com
 */
public interface StudentDao {
    // 获取表格列数据
    String[] getTableColumns();
}
