package com.cskaoyan.javase.swing.manager.stage2.dao.impl;

import com.cskaoyan.javase.swing.manager.stage2.dao.StudentDao;
import com.cskaoyan.javase.swing.manager.stage2.model.Student;
import com.cskaoyan.javase.swing.manager.stage2.model.StudentData;

/**
 * 与学生Student相关的，所有数据处理都在该类下进行
 *
 * @since 14:26
 * @author wuguidong@cskaoyan.onaliyun.com
 */
public class StudentDaoImpl implements StudentDao {

    // 从数据源获取数据
    private Student[] STUDS = StudentData.STUDS;
    private String[] COLUMNS = StudentData.COLUMNS;

    @Override
    public String[] getTableColumns() {
        return COLUMNS;
    }

}
