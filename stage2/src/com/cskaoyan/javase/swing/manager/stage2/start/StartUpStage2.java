package com.cskaoyan.javase.swing.manager.stage2.start;

import com.cskaoyan.javase.swing.manager.stage2.view.LoginFrame;

/**
 * 从此启动整个程序
 * @since 14:50
 * @author wuguidong@cskaoyan.onaliyun.com
 */
public class StartUpStage2 {
    public static void main(String[] args) {
        new LoginFrame();
    }
}
