package com.cskaoyan.javase.swing.manager.stage3.controller.impl;

import com.cskaoyan.javase.swing.manager.stage3.controller.StudentController;
import com.cskaoyan.javase.swing.manager.stage3.dao.StudentDao;
import com.cskaoyan.javase.swing.manager.stage3.dao.impl.StudentDaoImpl;
import com.cskaoyan.javase.swing.manager.stage3.model.Student;

/**
 * 与学生信息相关的,业务操作的具体实现
 * @since 19:30
 * @author wuguidong@cskaoyan.onaliyun.com
 */
public class StudentControllerImpl implements StudentController {

    // 业务处理需要获取数据,所以需要依赖数据处理层
    private StudentDao studentDao = new StudentDaoImpl();

    /**
     * Swing GUI表格需要的表格数据data是一个String类型的二维数组,
     *      所以需要封装出二维数组
     * @since 9:19
     * @return java.lang.String[][] 作为GUI表格的数据data
     * @author wuguidong@cskaoyan.onaliyun.com
     */
    @Override
    public String[][] getAllTableData() {
        // 获取数据
        Student[] realStudents = studentDao.getRealStudents();
        return get2DStrArrByStudentArr(realStudents);
    }

    /**
     *  Swing GUI表格需要的表格列是一个String数组
     * @since 9:20
     * @return java.lang.String[]
     * @author wuguidong@cskaoyan.onaliyun.com
     */
    @Override
    public String[] getTableColumns() {
        return studentDao.getTableColumns();
    }


    /**
     * 检查id是否重复,true表示重复,否则为不重复
     * @since 18:11
     * @param id 学生id
     * @return boolean
     * @author wuguidong@cskaoyan.onaliyun.com
     */
    @Override
    public boolean checkStuIdRepeat(String id) {
        if (id == null || "".equals(id)) {
            // 若输入的id不存在,效果等同于重复,禁止插入
            return true;
        }
        return studentDao.checkStuIdRepeat(id);
    }

    /**
     * 导入一条学生信息，true表示成功，否则为失败
     * 由于已经判定过很多次了，所以失败只有一种可能，即数组满了
     * @since 14:21
     * @param stu GUI界面输入的学生对象
     * @return boolean
     * @author wuguidong@cskaoyan.onaliyun.com
     */
    @Override
    public boolean addStudent(Student stu) {
        if (stu == null) {
            // 传入null，直接算插入失败
            return false;
        }
        return studentDao.addStudent(stu);
    }


    /**
     * 根据界面传入的id来删除用户,方法会返回一个布尔值
     *      true表示删除成功,否者为删除失败
     * @since 9:21
     * @param id 界面传来的id
     * @return boolean
     * @author wuguidong@cskaoyan.onaliyun.com
     */
    @Override
    public boolean delStudent(String id) {
        // 这个方法需要你自己完成！
        return false;
    }

    /**
     * Swing表格中的数据,需要的是一个String类型二维数组,所以通过学号id去数据源查找一条学生信息,
     * 如果存在就返回一个长度为1,装有学生信息的String二维数组
     * 如果不存在就返回null
     * 注意: 学号是唯一的,所以只要查询到了,必然是一条数据
     * @since 20:35
     * @param stuId 查询使用的学号
     * @return java.lang.String[][]
     * @author wuguidong@cskaoyan.onaliyun.com
     */
    @Override
    public String[][] getResultByStuId(String stuId) {
        // 这个方法需要你自己完成！
        return null;
    }

    /**
     * Swing表格中的数据,需要的是一个String类型二维数组,通过name去数据源查找学生信息,如果存在就返回一个String类型二维数组,不存在就返回null
     * 注意: 姓名不是唯一的,所以从数据源查出来的可能不只是一条数据,
     *      这时方法返回的二维数组的长度就不再一定是长度为1了.
     *
     * @since 12:10
     * @param name 被检索的name
     * @return java.lang.String[][]
     * @author wuguidong@cskaoyan.onaliyun.com
     */
    @Override
    public String[][] getResultByName(String name) {
        // 这个方法需要你自己完成！
        return null;
    }


    /**
     * 将一个学生对象数组转换成表格需要的数据,即该学生对象数组长度的一个二维数组
     * @since 12:21
     * @param stus 目标学生数组
     * @return java.lang.String[][]
     * @author wuguidong@cskaoyan.onaliyun.com
     */
    private String[][] get2DStrArrByStudentArr(Student[] stus) {
        // 获取结果二维数组的长度
        String[][] result = new String[stus.length][];
        // 遍历二维数组并赋值
        /*
            赋值很简单,因为这个二维数组的长度一定是学生数组的长度
            并且每个String数组的长度一定是表格的列数,也是固定的
            每个String数组的内容也是固定的
         */
        for (int i = 0; i < result.length; i++) {
            Student aStu = stus[i];
            result[i] = new String[]{
                    aStu.getStuId(), aStu.getName(), aStu.getGender(), aStu.getSchool(), aStu.getMajor(), aStu.getAge(), aStu.getCity(), aStu.getPhone(), aStu.getEmail()};
        }
        return result;
    }

}
