package com.cskaoyan.javase.swing.manager.stage3.dao;

import com.cskaoyan.javase.swing.manager.stage3.model.Student;

/**
 * 与Student学生数据相关的，数据操作的接口
 *
 * @since 14:21
 * @author wuguidong@cskaoyan.onaliyun.com
 */
public interface StudentDao {
    // 从数据源中获取学生信息,需要去除数组中为null的元素
    Student[] getRealStudents();

    // 获取表格列数据
    String[] getTableColumns();

    // 判断学号的唯一性
    boolean checkStuIdRepeat(String id);

    // 新增学生，插入一条数据
    boolean addStudent(Student stu);

}
