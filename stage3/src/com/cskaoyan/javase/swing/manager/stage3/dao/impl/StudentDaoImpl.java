package com.cskaoyan.javase.swing.manager.stage3.dao.impl;

import com.cskaoyan.javase.swing.manager.stage3.dao.StudentDao;
import com.cskaoyan.javase.swing.manager.stage3.model.Student;
import com.cskaoyan.javase.swing.manager.stage3.model.StudentData;

/**
 * 与学生Student相关的，所有数据处理都在该类下进行
 *
 * @since 14:26
 * @author wuguidong@cskaoyan.onaliyun.com
 */
public class StudentDaoImpl implements StudentDao {

    // 从数据源获取数据
    private Student[] STUDS = StudentData.STUDS;
    private String[] COLUMNS = StudentData.COLUMNS;

    /**
     * 数组中有很多null元素,为了避免空指针异常
     *      该方法筛选中其中非null元素构成一个新的学生数组,作为返回值
     * @since 9:57
     * @return com.cskaoyan.javase.swing.manager.stage2.model.Student[]
     * @author wuguidong@cskaoyan.onaliyun.com
     */
    @Override
    public Student[] getRealStudents() {
        // 获取数据
        // 确定不为null学生元素的个数
        int count = 0;
        for (Student stu : STUDS) {
            if (stu != null) {
                count++;
            }
        }
        Student[] result = new Student[count];
        int index = 0;
        for (Student stu : STUDS) {
            if (stu != null) {
                result[index] = stu;
                index++;
            }
        }
        return result;
    }

    @Override
    public String[] getTableColumns() {
        return COLUMNS;
    }

    /**
     * 检查id是否重复,true表示id重复,false为不重复
     * @since 18:09
     * @param id 传入学生id
     * @return boolean true表示id重复,false为不重复
     * @author wuguidong@cskaoyan.onaliyun.com
     */
    @Override
    public boolean checkStuIdRepeat(String id) {
        Student[] realStudents = getRealStudents();
        for (Student realStudent : realStudents) {
            if (realStudent.getStuId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 插入,由于数组长度不可改变
     *      所以逻辑是将数组中的null元素变为一个对象
     * @since 14:26
     * @param stu 被插入的非空学生
     * @return boolean true表示插入成功
     * @author wuguidong@cskaoyan.onaliyun.com
     */
    @Override
    public boolean addStudent(Student stu) {
        for (int i = 0; i < STUDS.length; i++) {
            if (STUDS[i] == null) {
                STUDS[i] = stu;
                return true;
            }
        }
        return false;
    }
}
