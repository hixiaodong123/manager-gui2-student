package com.cskaoyan.javase.swing.manager.stage3.start;

import com.cskaoyan.javase.swing.manager.stage3.view.LoginFrame;

/**
 * 从此启动整个程序
 * @since 14:50
 * @author wuguidong@cskaoyan.onaliyun.com
 */
public class StartUpStage3 {
    public static void main(String[] args) {
        new LoginFrame();
    }
}
