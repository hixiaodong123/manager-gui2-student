package com.cskaoyan.javase.swing.manager.stage4.start;

import com.cskaoyan.javase.swing.manager.stage4.view.LoginFrame;

/**
 * 从此启动整个程序
 * @since 14:50
 * @author wuguidong@cskaoyan.onaliyun.com
 */
public class StartUpStage4 {
    public static void main(String[] args) {
        new LoginFrame();
    }
}
