package com.cskaoyan.javase.swing.manager.stage5.controller;

import com.cskaoyan.javase.swing.manager.stage5.model.Student;


/**
 * 与学生信息相关的,业务操作接口
 * @since 19:10
 * @author wuguidong@cskaoyan.onaliyun.com
 */
public interface StudentController {

    // 获取表格需要的学生信息二维数组
    String[][] getAllTableData();

    // 获取表格列数据
    String[] getTableColumns();

    // 检查学号重复性
    boolean checkStuIdRepeat(String id);

    // 插入一条学生信息
    boolean addStudent(Student stu);

    // 删除一条学生信息
    boolean delStudent(String id);

    // 检索: 通过学号获取一条表格数据
    String[][] getResultByStuId(String stuId);

    // 检索: 通过学号获取一条表格数据
    String[][] getResultByName(String name);

    // 修改一个单元格的内容
    boolean updateCellByStuId(String targetStuId, int targetCol, String newValue);

    // 修改一条学生信息
    int updateStudentByStuId(String targetStuId, Student stu);

    // 按照学号对全体数据进行升序排列
    String[][] ascendingSortById();

    // 按照年龄对全体数据进行升序排列
    String[][] descendingSortByAge();

    // 综合排序
    String[][] totalSort();
}
