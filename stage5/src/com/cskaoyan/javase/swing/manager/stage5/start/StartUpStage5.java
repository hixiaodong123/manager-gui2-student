package com.cskaoyan.javase.swing.manager.stage5.start;

import com.cskaoyan.javase.swing.manager.stage5.view.LoginFrame;

/**
 * 从此启动整个程序
 * @since 14:50
 * @author wuguidong@cskaoyan.onaliyun.com
 */
public class StartUpStage5 {
    public static void main(String[] args) {
        new LoginFrame();
    }
}
